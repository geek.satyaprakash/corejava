package com.deloitte.form;

import java.time.LocalDate;

public class EmployeeForm {
	private String emploeeId;
	private String employeeName;
	private LocalDate dateOfBirth;
	private AddressForm address;
	private String department;
	
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getEmploeeId() {
		return emploeeId;
	}
	public void setEmploeeId(String emploeeId) {
		this.emploeeId = emploeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public AddressForm getAddress() {
		return address;
	}
	public void setAddress(AddressForm address) {
		this.address = address;
	}
	
	
}
