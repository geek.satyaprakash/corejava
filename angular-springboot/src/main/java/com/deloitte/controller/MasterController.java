package com.deloitte.controller;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("master")
public class MasterController extends BaseController {

	@GetMapping("data")
	public Map<String, Map<String, String>> getCountryList() {

		Map<String, String> country = new HashMap<>();
		country.put("IN", "India");
		country.put("US", "USA");
		country.put("AU", "Austrelia");

		Map<String, String> department = new HashMap<>();
		department.put("1M", "Core Engineering");
		department.put("2M", "Cloud Engineering");
		department.put("3U", "BFSI");

		Map<String, Map<String, String>> data = new HashMap<>();
		data.put("countries", country);
		data.put("departments", department);

		return data;

	}

	@GetMapping("cities/{countryCode}")
	public Map<String, String> getCitiList(@PathVariable("countryCode") String countryCode) {

		Map<String, String> cities = new HashMap<>();
		switch (countryCode) {
		case "IN":
			cities.put("S1", "Banglore");
			cities.put("S2", "Bhubaneswar");
			cities.put("S3", "Mumbai");
			cities.put("S4", "Delhi");
			break;
		case "US":
			cities.put("U1", "New York");
			cities.put("U2", "California");
			cities.put("U3", "Texas");
			break;
		case "AU":
			cities.put("A1", "Victoria");
			cities.put("A2", "Queensland");
			cities.put("A3", "South Australia");
			break;

		default:
			break;
		}

		return cities;

	}
	
	
}
