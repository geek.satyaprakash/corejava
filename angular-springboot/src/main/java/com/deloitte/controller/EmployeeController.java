package com.deloitte.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.form.EmployeeForm;

@RestController("employee")
public class EmployeeController extends BaseController {
	@PostMapping(name = "saveEmployee", consumes = "application/json")
	public void saveEmployee(@RequestBody EmployeeForm form) {
		System.out.println(form);
	}
}
