package com.deloitte.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@RestController
public class JsonReaderController {

	@PostMapping(name = "formatJSON", consumes = "text/plain")
	public String readJson(@RequestBody String jsonObject) {
		String s = "{\"d\":\"s\",\"dfef\":{\"dds\":8992,\"fdf\":{\"99\":{\"ll\":939239},\"dsdsd\":9823}},\"kfdf\":[{\"kk1\":9932398},{\"kk\":9998},{\"kk2\":9998}],\"normalArray\":[1,2,4,5,7]}";
		String s2 = "{\"d\":\"s\",\"dfef\":{\"dds\":8992,\"fdf\":{\"99\":{\"ll\":939239,\"ll1\":939239999},\"dsdsd\":9823}},\"kfdf\":[{\"kk1\":9932398},{\"kk\":9998},{\"kk2\":9998},{\"kk4\":9998888}],\"normalArray\":[1,2,4,5,7,8]}";
		String s3 = "\"[{\\\"d\\\":\\\"s\\\",\\\"dfef\\\":{\\\"dds\\\":8992,\\\"fdf\\\":{\\\"99\\\":{\\\"ll\\\":939239,\\\"ll1\\\":939239999},\\\"dsdsd\\\":9823}},\\\"kfdf\\\":[{\\\"kk\\\":9998},{\\\"kk2\\\":9998},{\\\"kk4\\\":9998888}],\\\"normalArray\\\":[1,2,4,5,7,8]},{\\\"d\\\":\\\"s\\\",\\\"dfef\\\":{\\\"dds\\\":8992,\\\"fdf\\\":{\\\"99\\\":{\\\"ll\\\":939239,\\\"ll1\\\":939239999},\\\"dsdsd\\\":9823}},\\\"kfdf\\\":[{\\\"kk\\\":9998},{\\\"kk2\\\":9998},{\\\"kk4\\\":9998888}],\\\"normalArray\\\":[1,2,4,5,7,8]}]\"";
		JsonParser parser = new JsonParser();
		JsonElement obj = parser.parse(s);
		JsonElement obj2 = parser.parse(s2);
		JsonElement obj3 = parser.parse(jsonObject);
		Map<String, Object> hirarchyObj = new HashMap<>();
		Map<Long, Object> savePointObj = new HashMap<>();
		convertToHierarchy(obj, hirarchyObj, savePointObj);
		convertToHierarchy(obj2, hirarchyObj, savePointObj);
		convertToHierarchy(obj3, hirarchyObj, savePointObj);
		return hirarchyObj.toString() + " * " + savePointObj;
	}

	public Map<String, Object> convertToHierarchy(JsonElement jsonElement, Map<String, Object> tabHirarchyObj,
			Map<Long, Object> savePointObj) {
		if (jsonElement.isJsonObject()) {
			JsonObject jsonObj = jsonElement.getAsJsonObject();
			Set<String> keys = jsonObj.keySet();
			for (String key : keys) {
				if (jsonObj.get(key).isJsonObject()) {
					Map<String, Object> subTabHirarchyObj = null;
					subTabHirarchyObj = checkAndCreateTab(tabHirarchyObj, key);

					convertToHierarchy(jsonObj.get(key), subTabHirarchyObj, savePointObj);
				} else if (jsonObj.get(key).isJsonArray()) {
					JsonArray arrs = jsonObj.get(key).getAsJsonArray();
					AtomicBoolean isNoramlArray = new AtomicBoolean(false);
					arrs.forEach(arr -> {
						if (arr.isJsonObject() || arr.isJsonArray()) {
							isNoramlArray.set(false);
						} else {
							isNoramlArray.set(true);
						}
					});
					if (isNoramlArray.get()) {
						addMultiSelectValue(tabHirarchyObj, savePointObj, jsonObj, key);
					} else {
						Map<String, Object> subTabhirarchyObj = checkAndCreateTab(tabHirarchyObj, key);
						convertToHierarchy(jsonObj.get(key), subTabhirarchyObj, savePointObj);
					}
				} else {
					addToValueObject(tabHirarchyObj, savePointObj, jsonObj, key);
				}
			}
		} else if (jsonElement.isJsonArray()) {
			JsonArray arrs = jsonElement.getAsJsonArray();
			arrs.forEach(arr -> convertToHierarchy(arr, tabHirarchyObj, savePointObj));
		}
		return tabHirarchyObj;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> checkAndCreateTab(Map<String, Object> tabHirarchyObj, String key) {
		Map<String, Object> subTabhirarchyObj;
		if (tabHirarchyObj.containsKey(key)) {
			subTabhirarchyObj = (Map<String, Object>) tabHirarchyObj.get(key);
		} else {
			subTabhirarchyObj = new HashMap<>();
			tabHirarchyObj.put(key, subTabhirarchyObj);
		}
		return subTabhirarchyObj;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void addToValueObject(Map<String, Object> tabHirarchyObj, Map<Long, Object> savePointObj,
			JsonObject jsonObj, String key) {
		if (savePointObj.containsKey(tabHirarchyObj.get(key))) {
			((List) savePointObj.get(tabHirarchyObj.get(key))).add(jsonObj.get(key));
		} else {
			List ll = new ArrayList();
			long valueObjKey = savePointObj.size() + 1;
			ll.add(jsonObj.get(key));
			savePointObj.put(valueObjKey, ll);
			tabHirarchyObj.put(key, valueObjKey);
		}
	}
	
	private void addMultiSelectValue(Map<String, Object> tabHirarchyObj, Map<Long, Object> savePointObj,JsonObject jsonObj, String key) {
		if (savePointObj.containsKey(tabHirarchyObj.get(key))) {
			((List) savePointObj.get(tabHirarchyObj.get(key))).add(jsonObj.get(key));
		} else {
			List list = new ArrayList();
			long valueObjKey = savePointObj.size() + 1;
			LinkedList ll=new LinkedList<>();
			jsonObj.get(key).getAsJsonArray().forEach(arr->ll.add(arr));
			list.add(ll);
			savePointObj.put(valueObjKey, list);
			tabHirarchyObj.put(key, valueObjKey);
		}
	}
}
