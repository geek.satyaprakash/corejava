package com.deloitte.entity;

import java.time.LocalDate;

public class Employee {
	private String emploeeId;
	private String employeeName;
	private LocalDate dateOfBirth;
	private Address address;
	private String department;

	public String getEmploeeId() {
		return emploeeId;
	}

	public void setEmploeeId(String emploeeId) {
		this.emploeeId = emploeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

}
